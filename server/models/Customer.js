const mongoose=require('mongoose');

const CustomerGeneralInformation = require('./CustomerGeneralInformation').schema;
const CustomerContact = require('./CustomerContact').schema;
const CustomerFinance=require('./CustomerFinancial').schema;
const CustomerPersonalData=require('./CustomerPersonalData').schema;
const CustomerIdentification=require('./CustomerIdentification').schema;


const Schema=mongoose.Schema;


const CustomerSchema=new Schema({
  

   generalInformation: [CustomerGeneralInformation],
    
    contactData: [CustomerContact],
    financialData:[CustomerFinance],
    personalData:[CustomerPersonalData],
    identificationData:[CustomerIdentification]
});

module.exports=mongoose.model('Customer',CustomerSchema);