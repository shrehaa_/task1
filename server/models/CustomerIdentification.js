const mongoose=require('mongoose');
const Schema=mongoose.Schema;


const CustomerIdenticationSchema=new Schema({
    customerId:String,
    birthDay: String,
    countryBirth: String,
    countryPrimCitizen: String,
    countrySecCitizen: String,
    isUsCitizen: Number,
    birthPlace: String,
    birthName: String,
    idCitizenship: String,
    idDocType: String,
    idNo: Number,
    idAuthority: String,
    idCountry: String,
    idCountryOfDocument: String,
    idIssued: String,
    idValidUntil: String


});



module.exports=mongoose.model('CustomerIdentification',CustomerIdenticationSchema);