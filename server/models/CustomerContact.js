const mongoose=require('mongoose');
const Schema=mongoose.Schema;


const CustomerContactSchema=new Schema({
    customerId:String,
    phone: String,
    email: String,
    countryResidence: String,
    mailbox: String,
    houseNo: String,
    zip: String,
    city: String


});



module.exports=mongoose.model('CustomerContact',CustomerContactSchema);