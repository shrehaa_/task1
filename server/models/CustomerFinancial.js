const mongoose=require('mongoose');
const Schema=mongoose.Schema;


const CustomerFinancialSchema=new Schema({
    customerId:String,
    taxNo: String,
    countryTaxation: String,
    refBankIban: String,
    StringrefBankBic: String,
    refBankAccOwner: String


});



module.exports=mongoose.model('CustomerFinance',CustomerFinancialSchema);