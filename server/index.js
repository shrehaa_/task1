const express=require('express');
const mongoose=require('mongoose')
const {graphqlHTTP}=require('express-graphql')
const schema=require('./Schema/schema')
const cors = require('cors');


const app=express();
app.use(cors());


mongoose.connect('mongodb+srv://node:mongodb@cluster0.lg09k.gcp.mongodb.net/employeeDB?retryWrites=true&w=majority');
mongoose.connection.once('open',()=>{
  console.log('Database Connected');
});

app.use('/graphql', graphqlHTTP({

  schema,
  pretty: true,
  graphiql: true
}));

app.listen(5000,()=>{
    console.log('Listening to Port 5000');
})