const graphql=require('graphql');
const customer =require('../models/Customer');
const genInfo=require('../models/CustomerGeneralInformation')
const cusContact=require('../models/CustomerContact');
const cusFin=require('../models/CustomerFinancial');
const cusIdent=require('../models/CustomerIdentification');
const cusPer=require('../models/CustomerPersonalData');


const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLInputObjectType} = graphql;

const generalInformationType=new GraphQLObjectType({
    name:'generalInformation',
    fields:()=>({
        purposeOfBusiness:{type: GraphQLString},
     })
});

const generalInformationInputType=new GraphQLInputObjectType({
    name:'generalInformationInput',
    fields:()=>({
        purposeOfBusiness:{type: GraphQLString},
     })
});

const contactDataType=new GraphQLObjectType({
    name:'contactData',
    fields:()=>({
        phone: {type:GraphQLString},
        email: {type:GraphQLString},
        countryResidence: {type:GraphQLString},
        mailbox: {type:GraphQLString},
        houseNo: {type:GraphQLString},
        zip: {type:GraphQLString},
        city: {type:GraphQLString}
    })

});

const contactDataTypeInput=new GraphQLInputObjectType({
    name:'contactDataInput',
    fields:()=>({
        phone: {type:GraphQLString},
        email: {type:GraphQLString},
        countryResidence: {type:GraphQLString},
        mailbox: {type:GraphQLString},
        houseNo: {type:GraphQLString},
        zip: {type:GraphQLString},
        city: {type:GraphQLString}
    })

});

const financialDataType=new GraphQLObjectType({
    name:'financialData',
    fields:()=>({
        taxNo: {type:GraphQLString},
        countryTaxation: {type:GraphQLString},
        refBankIban: {type:GraphQLString},
        StringrefBankBic:{type:GraphQLString},
        refBankAccOwner: {type:GraphQLString}
    })

});

const financialDataTypeInput=new GraphQLInputObjectType({
    name:'financialDataInput',
    fields:()=>({
        taxNo: {type:GraphQLString},
        countryTaxation: {type:GraphQLString},
        refBankIban: {type:GraphQLString},
        StringrefBankBic:{type:GraphQLString},
        refBankAccOwner: {type:GraphQLString}
    })

});

const personalDataType=new GraphQLObjectType({
    name:'personalData',
    fields:()=>({
        occupation: {type:GraphQLString},
        degree: {type:GraphQLString}
    })

});

const personalDataTypeInput=new GraphQLInputObjectType({
    name:'personalDataInput',
    fields:()=>({
        occupation: {type:GraphQLString},
        degree: {type:GraphQLString}
    })

});

const identificationDataType=new GraphQLObjectType({
    name:'identificationData',
    fields:()=>({
        birthDay: {type:GraphQLString},
        countryBirth: {type:GraphQLString},
        countryPrimCitizen: {type:GraphQLString},
        countrySecCitizen: {type:GraphQLString},
        isUsCitizen: {type:GraphQLInt},
        birthPlace: {type:GraphQLString},
        birthName: {type:GraphQLString},
        idCitizenship: {type:GraphQLString},
        idDocType: {type:GraphQLString},
        idNo: {type:GraphQLInt},
        idAuthority: {type:GraphQLString},
        idCountry:{type:GraphQLString},
        idCountryOfDocument: {type:GraphQLString},
        idIssued: {type:GraphQLString},
        idValidUntil: {type:GraphQLString}
    })

});

const identificationDataTypeInput=new GraphQLInputObjectType({
    name:'identificationDataInput',
    fields:()=>({
        birthDay: {type:GraphQLString},
        countryBirth: {type:GraphQLString},
        countryPrimCitizen: {type:GraphQLString},
        countrySecCitizen: {type:GraphQLString},
        isUsCitizen: {type:GraphQLInt},
        birthPlace: {type:GraphQLString},
        birthName: {type:GraphQLString},
        idCitizenship: {type:GraphQLString},
        idDocType: {type:GraphQLString},
        idNo: {type:GraphQLInt},
        idAuthority: {type:GraphQLString},
        idCountry:{type:GraphQLString},
        idCountryOfDocument: {type:GraphQLString},
        idIssued: {type:GraphQLString},
        idValidUntil: {type:GraphQLString}
    })

});

const CustomerType=new GraphQLObjectType({
    name:'Customer',
    fields:()=>({
        id:{type: GraphQLID},
        generalInformation:{type: generalInformationType},
        contactData:{type:contactDataType},
        financialData:{type:financialDataType},
        personalData:{type:personalDataType},
        identificationData:{type:identificationDataType}
          })
});

const RootQuery=new GraphQLObjectType({
    name:'RootQueryType',
    fields:{
        customers:{
            type: new GraphQLList(CustomerType),
            resolve(parent,args){
             
            return customer.find({});
            
            }
        }
    }
});

const Mutation=new GraphQLObjectType({
    name: 'Mutation',
    fields:{
        addGeneralInfo:{
           type: generalInformationType,
           args:{
               purposeOfBusiness:{type: GraphQLString}
           },
           resolve(parent,args){
               let gen=new genInfo({
                purposeOfBusiness:args.purposeOfBusiness
                

               });

               return gen.save();
                    
               }
           
            },
        addContactData:{
            type: contactDataType,
            args:{
                phone: {type:GraphQLString},
                email: {type:GraphQLString},
                countryResidence: {type:GraphQLString},
                mailbox: {type:GraphQLString},
                houseNo: {type:GraphQLString},
                zip: {type:GraphQLString},
                city: {type:GraphQLString}

            },
            resolve(parent,args){
                let contact=new cusContact({
                    phone:args.phone,
                    email:args.email,
                    countryResidence:args.countryResidence,
                    mailbox:args.mailbox,
                    houseNo:args.houseNo,
                    zip:args.zip,
                    city:args.city
                });
                return contact.save();
            }
        },
        addCustomer:{
            type: CustomerType,
            args:{
                generalInformation:{type: generalInformationInputType},
                contactData:{type:contactDataTypeInput},
                financialData:{type:financialDataTypeInput},
                personalData:{type:personalDataTypeInput},
                identificationData:{type:identificationDataTypeInput}
            },
            resolve(parent,args){
                let cust=new customer({
                    generalInformation:args.generalInformation,
                    contactData:args.contactData,
                    financialData:args.financialData,
                    personalData:args.personalData,
                    identificationData:args.identificationData
                });
                return cust.save();
            }
        },
        addfinancialData:{
            type: financialDataType,
            args:{
                taxNo: {type:GraphQLString},
                countryTaxation: {type:GraphQLString},
                refBankIban: {type:GraphQLString},
                StringrefBankBic:{type:GraphQLString},
                refBankAccOwner: {type:GraphQLString}

            },
            resolve(parent,args){
                let fin=new cusFin({
                    taxNo:args.taxNo,
                    countryTaxation:args.countryTaxation,
                    refBankIban:args.refBankIban,
                    StringrefBankBic:args.StringrefBankBic,
                    refBankAccOwner:args.refBankAccOwner
                });
                return fin.save();
            }
        },
        addpersonalData:{
            type:personalDataType,
            args:{
                occupation: {type:GraphQLString},
                degree: {type:GraphQLString}
            },
            resolve(parent,args){
                let pers=new cusPer({
                    occupation:args.occupation,
                    degree:args.occupation
                });

                return pers.save();
            }
        },
        addidentificationData:{
            type:identificationDataType,
            args:{
                birthDay: {type:GraphQLString},
                countryBirth: {type:GraphQLString},
                countryPrimCitizen: {type:GraphQLString},
                countrySecCitizen: {type:GraphQLString},
                isUsCitizen: {type:GraphQLInt},
                birthPlace: {type:GraphQLString},
                birthName: {type:GraphQLString},
                idCitizenship: {type:GraphQLString},
                idDocType: {type:GraphQLString},
                idNo: {type:GraphQLInt},
                idAuthority: {type:GraphQLString},
                idCountry:{type:GraphQLString},
                idCountryOfDocument: {type:GraphQLString},
                idIssued: {type:GraphQLString},
                idValidUntil: {type:GraphQLString}

            },
            resolve(parent,args){
                let iden=new cusIdent({
                    birthDay:args.birthDay,
                    countryBirth:args.countryBirth,
                    countryPrimCitizen:args.countryPrimCitizen,
                    countrySecCitizen:args.countrySecCitizen,
                    isUsCitizen:args.isUsCitizen,
                    birthPlace:args.birthDay,
                    birthName:args.birthName,
                    idCitizenship:args.idCitizenship,
                    idDocType:args.idDocType,
                    idNo:args.idNo,
                    idAuthority:args.idAuthority,
                    idCountry:args.idCountry,
                    idCountryOfDocument:args.idCountryOfDocument,
                    idIssued:args.idIssued,
                    idValidUntil:args.idValidUntil


                });

                return iden.save();
            }
        },
        updateCustomer:{
            type: CustomerType,
            args:{
                id:{type: GraphQLID},
                generalInformation:{type: generalInformationInputType},
                contactData:{type:contactDataTypeInput},
                financialData:{type:financialDataTypeInput},
                personalData:{type:personalDataTypeInput},
                identificationData:{type:identificationDataTypeInput}
            },
            resolve(parent,args){
                return new Promise((resolve, reject)=>{
                    customer.findOneAndUpdate({"_id": args.id},
                     { "$set":  {generalInformation: args.generalInformation,
                                contactData:args.contactData,
                                financialData:args.financialData,
                                personalData:args.personalData,
                                identificationData:args.identificationData}},
                      {"new":true},
                      
                        ).exec((err, res)=>{
                            console.log('test', res)
                            //if(err) reject(err)
                            //else resolve(res)
                            resolve(res)
                        })

                })
              
            }
        }
    }
    
});

module.exports=new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})